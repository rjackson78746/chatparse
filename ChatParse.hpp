#ifndef CHAT_PARSE_HPP
#define CHAT_PARSE_HPP 
#include <string>
#include <vector>
#include <map>
#include <boost/regex.hpp>
#include <curl/curl.h>

namespace HipChat
{
  /// -------------------------------------------------------------------------------------------
  /// @brief Convert a HipChat string to a JSON string containing information about its contents
  /// Special content to look for includes:
  /// 1. @mentions - A way to mention a user. 
  ///    Always starts with an '@' and ends when hitting a non-word character. 
  /// 2. Emoticons - Only handles 'custom' emoticons which are ASCII strings, no longer than 15 characters, contained in parenthesis. 
  ///    Assume that anything matching this format is an emoticon. 
  /// 3. Links - Any URLs contained in the message, along with the page's title.
  ///
  /// General Flow
  /// -----------------
  /// Read in Chat msg
  ///   Parse special content
  ///     Go get HTML page title if necessary
  /// -------------------------------------------------------------------------------------------
  class ChatParse
  {
    public:
      ChatParse(); ///< default constructor
      ChatParse(const ChatParse &); ///< copy constructor
      ChatParse & operator=(const ChatParse &); ///< assignment operator
      ~ChatParse(); ///< destructor
      /// ------------------------------------------------------
      /// @brief tokenize chat string into identified contents
      ///        and store in internal containers
      /// @param[in] Chat string to be parsed
      /// @return error status code, true = no errors, false = error (see stderr print)
      /// ------------------------------------------------------
      bool parse(const std::string &);
      /// ------------------------------------------------------
      /// @brief internal debug print
      /// ------------------------------------------------------
      void print();
      /// ------------------------------------------------------
      /// @brief Reset internal container objects. Call this between uses
      ///        of a single instance of ChatParse
      /// ------------------------------------------------------
      void reset();
      /// ------------------------------------------------------
      /// @brief Reset internal URL cache.
      /// ------------------------------------------------------
      void resetCache();
      /// @brief get maxUrlAge
      int maxUrlAge() const
      {
        return m_maxUrlAge;
      }
      /// @brief set maxUrlAge
      void maxUrlAge(const int & m)
      {
        m_maxUrlAge = m;
      }
      /// @brief get page title
      std::string title() const
      {
        return m_title;
      }
      /// @brief set page title
      void title(const std::string & t)
      {
        m_title = t;
      }

      /// @brief get mentions regex
      boost::regex mentionsRegEx() const
      {
        return m_mentionsRegEx;
      }
      /// @brief set mentions regex
      void mentionsRegEx(boost::regex re)
      {
        m_mentionsRegEx = re;
      }

      /// @brief get emoticons regex
      boost::regex emoticonsRegEx() const
      {
        return m_emoticonsRegEx;
      }
      /// @brief set emoticons regex
      void emoticonsRegEx(boost::regex re)
      {
        m_emoticonsRegEx = re;
      }

      /// @brief get URL regex
      boost::regex urlsRegEx() const
      {
        return m_urlsRegEx;
      }
      /// @brief set URL regex
      void urlsRegEx(boost::regex re)
      {
        m_urlsRegEx = re;
      }
      
      /// ------------------------------------------------------
      /// @brief Return count of special tokens found in chat message
      /// ------------------------------------------------------
      int tokenCount() const
      {
        return m_mentions.size() + 
               m_emoticons.size() + 
               m_urls.size();
      }
      /// ------------------------------------------------------
      /// @brief Form JSON string from chat tokens
      /// @return JSON string with chat msg special content, empty string on failure
      /// ------------------------------------------------------
      std::string json() const;

    private:
      typedef int TIME;               ///< URL insertion time
      typedef std::string URL;        ///< URL string
      typedef std::string PAGE_TITLE; ///< HTML page title from URL

      struct UrlData          ///< record struct to hold URL data in cache
      {
        TIME updateTime;      ///< time at which this record was placed in the cache
        PAGE_TITLE pageTitle; ///< title of HTML page
      };

      /// @brief create JSON and return as stream
      friend std::ostream & operator<<(std::ostream &os, const ChatParse& p)
      {
        os << p.json();
        return os;
      }

      std::string              m_title;          ///< URL page title for current page being parsed
      std::vector<std::string> m_mentions;       ///< @mentions found in chatMsg
      std::vector<std::string> m_emoticons;      ///< emoticons found in chatMsg
      std::vector<std::string> m_urls;           ///< urls found in chatMsg
      boost::regex             m_mentionsRegEx;  ///< regex for parsing @mentions from chatMsg
      boost::regex             m_emoticonsRegEx; ///< regex for parsing emoticons from chatMsg
      boost::regex             m_urlsRegEx;      ///< regex for parsing URLs from chatMsg
      std::map<URL, UrlData>   m_urlCache;       ///< cache recent results from URL page retrieval,
                                                 ///< if URL is found again within m_maxUrlAge seconds, it
                                                 ///< will be retrieved from cache rather that hitting the URL again
      int m_maxUrlAge;                           ///< age age which URL cache entry becomes stale, 
                                                 ///< set to zero to always retrieve page title from external resource
      CURL * m_curl_handle;                      ///< handle to curl lib

      /// ------------------------------------------------------
      /// @brief Given a URL, GET the HTML and parse out the page title
      ///        Issues HTTP Get
      /// @param[in] URL from which we retrieve page title
      /// @return true = page title found, false = not page title found
      /// ------------------------------------------------------
      bool getPageTitle(const std::string &);
  };
}

#endif
