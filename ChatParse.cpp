
#include <boost/algorithm/string.hpp> 
#include <iostream>
#include <json/value.h>
#include <json/writer.h>
#include "ChatParse.hpp"

using namespace std;
using namespace HipChat;

ChatParse::ChatParse() : m_maxUrlAge(0), m_curl_handle(0)
{
  // init regular expressions to find special content in chat msg
  m_mentionsRegEx  = "@([\\w]+)"; // @ sign followed by ascii chars, no spaces
  m_emoticonsRegEx = "\\(([a-zA-Z]{1,15})\\)"; // 1 - 15 chars enclosed in parethesis
  m_urlsRegEx      = "http[s]?\\://[a-zA-Z0-9\\-\\.]+\\.[a-zA-Z]{2,3}(/[a-zA-Z0-9\\-\\.]*)*"; // http url
  // init curl lib, for inet access
  curl_global_init(CURL_GLOBAL_ALL);
  m_curl_handle = curl_easy_init();
}

ChatParse::ChatParse(const ChatParse & cp) : m_mentionsRegEx(cp.mentionsRegEx()),
                                             m_emoticonsRegEx(cp.emoticonsRegEx()),
                                             m_urlsRegEx(cp.urlsRegEx()),
                                             m_maxUrlAge(0), m_curl_handle(0)
{
  curl_global_init(CURL_GLOBAL_ALL);
  m_curl_handle = curl_easy_init();
}

ChatParse & ChatParse::operator=(const ChatParse & cp) ///< assignment operator
{
  mentionsRegEx(cp.mentionsRegEx());
  emoticonsRegEx(cp.emoticonsRegEx());
  urlsRegEx(cp.urlsRegEx());

  curl_global_init(CURL_GLOBAL_ALL);
  m_curl_handle = curl_easy_init();
  return *this;
}

ChatParse::~ChatParse()
{
  // cleanup curl stuff 
  curl_easy_cleanup(m_curl_handle);
  curl_global_cleanup();
}

bool ChatParse::parse(const std::string & chatMsg)
{
  bool ret = true;

  if(chatMsg.size() <= 0)
    return false;

  try
  {
    boost::smatch match;
    // parse mentions from input msg
    string tmp = chatMsg;
    while (boost::regex_search(tmp, match, m_mentionsRegEx)) 
    {
      // The first sub_match is the whole string; the next
      // sub_match is the first parenthesized expression.
      // We expect first paren expression to contain mention without the @ sign
      if (match.size() == 2) 
      {
        boost::ssub_match sub_match = match[1];

        // store match
        m_mentions.push_back(sub_match.str());
        // advance our position in the string in case there are multiple hits for this pattern
        tmp = match.suffix().str();
      }
    }

    // parse emoticons from input msg
    tmp = chatMsg;
    while (boost::regex_search(tmp, match, m_emoticonsRegEx)) 
    {
      // The first sub_match is the whole string; the next
      // sub_match is the first parenthesized expression.
      // We expect first paren expression to contain emoticon without the () signs
      if (match.size() == 2) 
      {
        boost::ssub_match sub_match = match[1];

        // store match
        m_emoticons.push_back(sub_match.str());
        // advance our position in the string in case there are multiple hits for this pattern
        tmp = match.suffix().str();
      }
    }

    // parse 'links' from input msg
    tmp = chatMsg;
    if (boost::regex_search(tmp, match, m_urlsRegEx)) 
    {
      // The first match is the whole URL string, that's what we want
      // 
      if (match.size() >= 1) 
      {
        bool getPage   = true; // hit URL for HTML if true
        // store match
        m_urls.push_back(match.str());

        // caching prevents us from repeatedly accessing the same URL if we already have the page title
        // Implementor of this object can control when a cache entry 'ages' out by setting maxUrlAge in seconds
        // Once the age of the cache entry exceeds that age, we re-access the URL to get the title again
        auto urlIt = m_urlCache.find(match.str());
        if(urlIt != m_urlCache.end()) // it's in the cache
        {
          if(time(NULL) - urlIt->second.updateTime <= m_maxUrlAge) // not aged out
          {
            cout << "Cache data age (seconds): " 
                 << time(NULL) - urlIt->second.updateTime << " Max: " 
                 << m_maxUrlAge << endl;
            cout << "Retrieving page title for URL: " << match.str() << " from cache\n";
            getPage = false;
            title(urlIt->second.pageTitle); // use cached page title
          }
        }

        if(getPage) // if true, we either a) don't have it, or b) it has age'ed out
        {
          cout << "Retrieving page title for URL from external site\n";
          if(getPageTitle(match.str())) // got it
          {
            UrlData cacheData;
            cacheData.updateTime    = time(NULL); // mark age as now
            cacheData.pageTitle     = title(); // store new/refreshed title
            m_urlCache[match.str()] = cacheData;
          }
          else
          {
            cerr << "Failed to retrieve HTML page title for URL: " << match.str() << endl;
          }
        }

        // advance our position in the string in case there are multiple hits for this pattern
        tmp = match.suffix().str();
      }
    }
  }
  catch (std::exception e)
  {
    cerr << "ChatParse::parse Caught Exception: " << e.what() << endl;
    ret = false;
  }
  return ret;
}

void ChatParse::print()
{
  try
  {
    cout << "--------- print --------------\n";
    cout << "\t--------- mentions --------------\n";
    for(auto it = m_mentions.cbegin(); it != m_mentions.cend(); ++it)
    {
      cout << *it << endl;
    }
    cout << "\t--------- emoticons --------------\n";
    for(auto it = m_emoticons.cbegin(); it != m_emoticons.cend(); ++it)
    {
      cout << *it << endl;
    }
    cout << "\t--------- urls --------------\n";
    for(auto it = m_urls.cbegin(); it != m_urls.cend(); ++it)
    {
      cout << *it << endl;
    }
    cout << "--------- end print --------------\n";
  }
  catch (std::exception e)
  {
    cerr << "ChatParse::print Caught Exception: " << e.what() << endl;
  }
}

// -----------------------------------------------------------------------------------
// 'C' function
// This is a libcurl callback executed by the curl library upon retrieval of data from the URL
// contents - page data
// size - bytes per nmemb
// nmemb - number of members
// userp - user supplied data - we supply an object instance of ChatParse so we can 
//         access member functions
// return bytes processed, zero on fail
// -----------------------------------------------------------------------------------
static size_t processPageCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  if(!contents || size <= 0 || nmemb <= 0)
    return 0;
  ChatParse * cpObj = static_cast<ChatParse*>(userp);
  if(!cpObj)
  {
    cerr << "Unable to convert userp to ChatParse Object, bailing...\n";
    return 0;
  }

  boost::smatch match;
  // parse page title from input msg
  string tmp;
  tmp.assign(static_cast<char*>(contents), size*nmemb);  // convert char * to std::string
  ///< regex for parsing HTML page title
  // <title> blah blah </title>
  boost::regex pageTitleRegEx(".*?<title>(.*?)</title>.*");
  
  if(boost::regex_search(tmp, match, pageTitleRegEx)) 
  {
    // The first sub_match is the whole string; the next
    // sub_match is the first parenthesized expression.
    if (match.size() == 2) {
      string sub_match = match[1];
      // convert common HTML escape sequences to ASCII
      boost::replace_all(sub_match, "&quot", "\"");
      boost::replace_all(sub_match, "&amp", "&");
      boost::replace_all(sub_match, "&gt", ">");
      boost::replace_all(sub_match, "&lt", "<");
      cpObj->title(sub_match); // store page title
    }
  }
  size_t realsize = size * nmemb;
  return realsize;
}

bool ChatParse::getPageTitle(const std::string & url)
{
  if(url.size() <= 0)
    return false;
  if(!m_curl_handle)
  {
    cerr << "ChatPars::getPageTitle Curl lib not initialized\n";
    return false;
  }
  try
  {
    CURLcode res;
    curl_easy_setopt(m_curl_handle, CURLOPT_URL, url.c_str());     // specify URL to get 
    curl_easy_setopt(m_curl_handle, CURLOPT_WRITEFUNCTION, processPageCallback); // set callback to handle Rx of HTTP request
    curl_easy_setopt(m_curl_handle, CURLOPT_WRITEDATA, this);      // set instance of ChatParse for use in 'C' callback 
    curl_easy_setopt(m_curl_handle, CURLOPT_FOLLOWLOCATION, true); // follow redirected links

    // some servers don't like requests that are made without a user-agent field, so we provide one 
    curl_easy_setopt(m_curl_handle, CURLOPT_USERAGENT, "libcurl-agent/1.0");

    // blocking call to retreive page 
    res = curl_easy_perform(m_curl_handle);

    // check for errors 
    if(res != CURLE_OK) 
    {
      cerr << "curl_easy_perform() failed: %s" << curl_easy_strerror(res) << endl;
      return false;
    }
    else 
    {
      return true; // found title, result of return code from processPageCallback() 
    }
  }
  catch (std::exception e)
  {
    cerr << "ChatParse::getPageTitle Caught Exception: " << e.what() << endl;
    return false;
  }
}

void ChatParse::reset()
{
  try
  {
    m_mentions.clear();
    m_emoticons.clear();
    m_urls.clear();
    m_title.clear();
  }
  catch (std::exception e)
  {
    cerr << "ChatParse::reset Caught Exception: " << e.what() << endl;
  }
}

void ChatParse::resetCache()
{
  try
  {
    m_urlCache.clear();
  }
  catch (std::exception e)
  {
    cerr << "ChatParse::resetCache Caught Exception: " << e.what() << endl;
  }
}

std::string ChatParse::json() const
{
  try
  {
    // iterate through containers and build json string
    Json::Value jsonRoot;
    Json::Value jsonMentions;
    Json::Value jsonEmoticons;
    Json::Value jsonLinks;

    if(m_mentions.size())
    {
      for(auto it = m_mentions.cbegin(); it != m_mentions.cend(); ++it)
      {
        jsonMentions.append(*it);
      }
      jsonRoot["mentions"] = jsonMentions;
    }

    if(m_emoticons.size())
    {
      for(auto it = m_emoticons.cbegin(); it != m_emoticons.cend(); ++it)
      {
        jsonEmoticons.append(*it);
      }
      jsonRoot["emoticons"] = jsonEmoticons;
    }

    if(m_urls.size())
    {
      Json::Value jsonLink;
      for(auto it = m_urls.cbegin(); it != m_urls.cend(); ++it)
      {
        jsonLink["url"] = *it;
        auto urlIt = m_urlCache.find(*it);
        if(urlIt != m_urlCache.end())
        {
          jsonLink["title"] = urlIt->second.pageTitle;
        }
        else
        {
          jsonLink["title"] = "";
        }
        jsonLinks.append(jsonLink);
        jsonLink.clear();
      }
      jsonRoot["links"] = jsonLinks;
    }

    Json::StyledWriter styledWriter;
    return styledWriter.write(jsonRoot);
  }
  catch (std::exception e)
  {
    cerr << "ChatParse::parse Caught Exception: " << e.what() << endl;
    return string();
  }
}
