#include <iostream>
#include "ChatParse.hpp"

using namespace std;
using namespace HipChat;

///@brief test driver for ChatParse class
int main(int argc, char ** argv)
{
  ChatParse chatParse;
  chatParse.maxUrlAge(2);
  bool ret = false;

  // test chat strings from Atlassian
  std::string s = "@chris you around?";
  ret = chatParse.parse(s);
  cout << "\ninput: \n" << s << endl;
  cout << "output (ret: " << ret << " tokens: " << chatParse.tokenCount() << "): \n" << chatParse << endl;

  chatParse.reset();
  s = "Good morning! (megusta) (coffee)";
  ret = chatParse.parse(s);
  cout << "\ninput: \n" << s << endl;
  cout << "output (ret: " << ret << " tokens: " << chatParse.tokenCount() << "): \n" << chatParse << endl;
  chatParse.reset();

  s = "Olympics are starting soon; http://www.nbcolympics.com";
  ret = chatParse.parse(s);
  cout << "\ninput: \n" << s << endl;
  cout << "output (ret: " << ret << " tokens: " << chatParse.tokenCount() << "): \n" << chatParse << endl;
  chatParse.reset();

  s = "@bob @john (success) such a cool feature; https://twitter.com/jdorfman/status/430511497475670016";
  ret = chatParse.parse(s);
  cout << "\ninput: \n" << s << endl;
  cout << "output (ret: " << ret << " tokens: " << chatParse.tokenCount() << "): \n" << chatParse << endl;
  chatParse.reset();

  // test page name caching
  // page result is cached for 2 seconds, after that it is retrieved again
  s = "some links http://www.example.com";
  for(int ix = 0; ix < 4; ++ix)
  {
    ret = chatParse.parse(s);
    cout << "\ninput: \n" << s << endl;
    cout << "output (ret: " << ret << " tokens: " << chatParse.tokenCount() << "): \n" << chatParse << endl;
    chatParse.reset();
    usleep(1000000);
  }

  // test a chat with a little of everything
  s = "Here is some text @rcj should like this, so should @maj and @raj check out http://www.example.com (mysmiley) (itsfriday)";
  ret = chatParse.parse(s);
  cout << "\ninput: \n" << s << endl;
  cout << "output (ret: " << ret << " tokens: " << chatParse.tokenCount() << "): \n" << chatParse << endl;
  chatParse.reset();

  // some negative tests
  // no input
  s = "";
  ret = chatParse.parse(s);
  cout << "\ninput: \n" << s << endl;
  cout << "output (ret: " << ret << " tokens: " << chatParse.tokenCount() << "): \n" << chatParse << endl;
  chatParse.reset();

  // bad URL
  s = "http://this is a bad URL/foo/bar";
  ret = chatParse.parse(s);
  cout << "\ninput: \n" << s << endl;
  cout << "output (ret: " << ret << " tokens: " << chatParse.tokenCount() << "): \n" << chatParse << endl;
  chatParse.reset();

  // emoticon > 15 chars
  s = "way too big emoticon (012345678901234567890)";
  ret = chatParse.parse(s);
  cout << "\ninput: \n" << s << endl;
  cout << "output (ret: " << ret << " tokens: " << chatParse.tokenCount() << "): \n" << chatParse << endl;
  chatParse.reset();
  return 0;
}

