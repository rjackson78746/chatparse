
Provded is the ChatParse class to demonstrate the parsing of a chat message containing special content.
Special content tokens are identified and parse from the chat message and formed into a JSON string.

Documenation
============
Run doxygen doxyfile.in to output source code documentation

Building
========
Requires cmake version 2.6 or later
From source code directory, run 'cmake .' to generate make file.
Run 'make' to build executable 'chatparse'.
'chatparse' executable is a driver which instantiates an instance of ChatParse and runs it through tests with various input strings.

External dependencies
=====================
ChatPare requires:
1. libcurl - used for retrieving page title data from web site
2. jsoncpp - used to form JSON message
3. boost   - used for regular expression and string manipulation
